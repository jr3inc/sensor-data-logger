#ifndef SDL_H
#define SDL_H

#include <string>
#include <Windows.h>

#include "JR3_PCI.h"
using namespace jr3;

namespace jr3{

	void startPCI(Jr3_PCI * aPCI)
	{
		std::string aDevID;
		bool done = false;
		bool success;
		int i = 0;
		int temp;
		const int MAX_TRIES = 30;

		cout << "Please enter the device ID: " ;
		cin >> aDevID;

		success = aPCI->setDeviceID(aDevID);
		cout << "Initializing PCI" << endl;
		cout << "reply code is " << aPCI->initPCI()<< endl;

		cout << "Pausing to finish system init." << endl;

		while (!done) {
			Sleep(100);
			temp = aPCI->getSerialNumber();
			if (temp == 0) {
				i++;
				done = (i > MAX_TRIES);
			}
			else
				done = true;
		}
		cout << "Serial number: " << aPCI->getSerialNumber()<< endl;
		cout << "Setting offsets..." << endl;
		aPCI->offsetInit();
		cout << "Offsets set." << endl << endl;

		return;
	};

	int getDelay()
	{
		int dv;

		cout << "Time interval between data readings (ms): ";
		cin >> dv;
		if (dv < 0)
			dv = 0;
		return dv;
	};

	std::string getFileName()
	{
		std::string myFile;
		cout << "Please enter the file name (ie data.txt): ";
		cin >> myFile;
		return myFile;
	}

}

#endif