#pragma once

#include <string>

class JR3_Time
{
public:
	JR3_Time(void);
	~JR3_Time(void);

	//accessors
	int getHour() {return hour;};
	int getMinute() {return minute;};
	int getSecond() {return second;};
	int getMilliseconds() {return millisecond;};
	int getYear() {return year;};
	int getMonth() {return month;};
	int getDay() {return day;};

	std::string shortMonthString(int);
	std::string monthString(int);

	std::string fullDate();
	std::string fullTime(int precision = 3);

	//mutators
	void now();
	void delay(int);	// in milliseconds. Windows won't delay < 17 ms.

private: 
	std::string toString(int num, int minWidth = 1);

	int year;
	int month;
	int day;
	int hour;
	int minute;
	int second;
	int millisecond;

};

