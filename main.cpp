#include <cstdlib>		// provides EXIT_SUCCESS
#include <string>
#include <iostream>
#include <fstream>
#include <conio.h>		//provides kbhit
#include "JR3_Time.h"
#include <time.h>
using namespace std;

#include <JR3_PCI.h>
#include "sdl.h"
using namespace jr3;

int main()
{
	enum TimeInterval {SECONDS, MINUTES, HOURS};

	Jr3_PCI aPCI("3113");
	int i, j;
	short k;
	unsigned int delayValue;
	char ch1, ch2;
	string fileName, countData;
	fstream outFile;
	bool done = false;
	double dataArray[6];
	bool goodEntry, displayToScreen;
	int numberOfSensors = 1;
	time_t startTime, stopTime;
	int runTimeUnits;
	int runTime;
	JR3_Time rightNow;

	startPCI(&aPCI);
	delayValue = getDelay();
	fileName = getFileName();

	if (aPCI.getProcessors() != numberOfSensors) {
		std::cout << "How many sensors are connected? ";
		std::cin >> numberOfSensors;
	}

	std::cout << "Display data to screen? y/n ";
	std::cin >> ch1;
	displayToScreen = (ch1 == 'y') || (ch1 == 'Y');


	i = 0;

	goodEntry = false;
	while (!goodEntry) {
		std::cout << "Will run time be in hours, minutes or seconds? h/m/s: " ;
		std::cin >> ch2;

		if (ch2== 'h' || ch2== 'H') {
			runTimeUnits = HOURS;
			goodEntry = true;
		}
		else if (ch2== 'm' || ch2== 'M'){
			runTimeUnits = MINUTES;
			goodEntry = true;
		}
		else if (ch2== 's' || ch2== 'S'){
			runTimeUnits = SECONDS;
			goodEntry = true;
		}
		else {
			std::cout << ch2 << " is not a valid entry." << endl;
		}
	}
	std::cout << "Enter the number of ";
	if (runTimeUnits == HOURS)
		std::cout << "hours";
	else if  (runTimeUnits == MINUTES)
		std::cout << "minutes";
	else
		std::cout << "seconds";
	std::cout << " the data should be collected: ";
	std::cin >> runTime;

	outFile.open(fileName, ios::out|ios::app);	//open and append
	for (k =0; k < numberOfSensors; k++)
		outFile << "Sensor S/N " << aPCI.getSerialNumber(k) << endl;
	outFile << "Time interval (ms): " << delayValue << endl;
	outFile << "Sensors connected: " << numberOfSensors << endl;
	if (runTimeUnits == HOURS) {
		outFile << "Run time: " << runTime << " hour";
		if (runTime > 1)
			outFile << "s";
	}
	else if  (runTimeUnits == MINUTES) {
		outFile << "Run time unit: " << runTime << " minute";
		if (runTime > 1)
			outFile << "s";
	}
	else {
		outFile << "Run time unit: " << runTime << " second";
		if (runTime > 1)
			outFile << "s";
	}
	outFile.close();

	std::cout << "Press enter to start." << endl;
	std::cout << "After starting, press 0 at any time to reset the offsets" << endl;
	std::cout << "Data will be collected until any other key is pressed" << endl;
	std::cout << "or until time runs out." << endl;
	std::cin.get();
	std::cin.ignore();

	//figure start time
	startTime = time(NULL);	//set startTime to now
	stopTime = startTime;
	if (runTimeUnits == HOURS)
		stopTime += 60*60*runTime;
	else if  (runTimeUnits == MINUTES)
		stopTime += 60*runTime;
	else
		stopTime += runTime;

	outFile.open(fileName, ios::out|ios::app);
	rightNow.now();
	outFile << endl;
	outFile << "Start date: " << rightNow.fullDate() << endl;
	outFile << "Start time: " << rightNow.fullTime() << endl;
	outFile << endl;
	outFile << "Clock\tFx\tFy\tFz\tMx\tMy\tMz\t";
	for (int i = 2; i <= numberOfSensors; i++)
		outFile << "Fx\tFy\tFz\tMx\tMy\tMz\t";
	outFile << endl;
	outFile.close();

	std::cout << "Data collection started." << endl;
	while (!done)
	{
		if (_kbhit())
		{
			ch2 = _getch();
			if (ch2 == 'o' || ch2 == 'O' || ch2 == '0')
			{
				for (k = 0; k < numberOfSensors; k++){
					aPCI.resetOffsets(k);
				}
				outFile.open(fileName, ios::out|ios::app);	//open and append
				outFile << "Offsets reset\n";
				outFile.close();
				std::cout << "Offsets reset" << endl << endl;
			}
			else
				done = true;
		}
		else
		{
			outFile.open(fileName, ios::out|ios::app);	//open and append
			rightNow.now();
			outFile << rightNow.fullTime(1) << '\t';

			for (k = 0; k < numberOfSensors; k++){

				for (j = 0; j < 6; j++) {
					dataArray[j] = aPCI.getScaledData(j, 3, k);
				}
				countData = aPCI.dataToString(dataArray[0], dataArray[1], dataArray[2], dataArray[3],
					dataArray[4], dataArray[5]);
				if (displayToScreen)
					std::cout << countData;
				outFile << countData;
				if (k < numberOfSensors - 1) {
					outFile << '\t';
					if (displayToScreen)
					std::cout << '\t';
				}
				if (aPCI.getFxCounts(k) == 0 && aPCI.getFyCounts(k) == 0 &&
					aPCI.getFzCounts(k) == 0 && aPCI.getMxCounts(k) == 0 &&
					aPCI.getMyCounts(k) == 0 && aPCI.getMzCounts(k) == 0)
					std::cout << "Check sensor " << k+1 << " -- all data is zero" << endl;

			}
			outFile << endl;
			outFile.close();
			aPCI.waitForData(delayValue);
			if (displayToScreen)
				std::cout << endl;
		};
		if (!done)
			done = ((time(NULL) - stopTime) >= 0.00);
	};

	outFile.open(fileName, ios::out|ios::app);
	rightNow.now();
	outFile << endl;
	outFile << "Stop date: " << rightNow.fullDate() << endl;
	outFile << "Stop time: " << rightNow.fullTime() << endl;
	outFile.close();

	aPCI.closePCI();
	std::cout << "Data collection finished." << endl;
	std::cin.ignore();
	std::cin.get();

	return EXIT_SUCCESS;
}