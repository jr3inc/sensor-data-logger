#include "JR3_Time.h"

#include <Windows.h>
#include <string>
#include <sstream>
#include <iomanip>
#include <time.h>
using namespace std;

JR3_Time::JR3_Time(void)
{
        now();
}


JR3_Time::~JR3_Time(void)
{
}

string JR3_Time::monthString(int whichMonth){
        string monthNames[13] = {"", "January", "February", "March", "April", "May", "June", "July", "August",
                "September", "October", "November", "December"};
        return monthNames[whichMonth];
}

string JR3_Time::shortMonthString(int whichMonth){
        string monthNames[13] = {"", "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug",
                "Sep", "Oct", "Nov", "Dec"};
        return monthNames[whichMonth];
}

string JR3_Time::fullDate(){
        string returnValue;
        returnValue = toString(day) + shortMonthString(month) + toString(year);
        return returnValue;
}

string JR3_Time::fullTime(int precision){
        string returnValue;
		  int temp_ms;

		  if (precision == 1)
			  temp_ms = millisecond / 100;
		  else if (precision == 2)
			  temp_ms = millisecond / 10;
		  else
			  temp_ms = millisecond;
        returnValue = toString(hour, 2) + ":" + toString(minute, 2) + ":" + toString(second, 2);
        returnValue = returnValue + "." + toString(temp_ms, precision);
        return returnValue;
}


void JR3_Time::now(){
        SYSTEMTIME time;
        GetLocalTime( &time );

        year = time.wYear;
        month = time.wMonth;
        day = time.wDay;
        hour = time.wHour;
        minute = time.wMinute;
        second = time.wSecond;
        millisecond = time.wMilliseconds;
}

string JR3_Time::toString(int num, int minWidth){
        ostringstream _int;
        _int << setfill ('0') << setw(minWidth);
        _int << num << flush;
        return _int.str();
}

void JR3_Time::delay(int ms){
        clock_t start_clock = clock();
        int h = clock();
        while (clock() < h + ms);
        return;
}